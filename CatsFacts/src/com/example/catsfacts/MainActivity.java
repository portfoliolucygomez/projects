package com.example.catsfacts;



import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends FragmentActivity {
	
	private static final int PAGES= 5;
	
	private ViewPager mPager;
	private PagerAdapter mPagerAdapter;
	Gson gson;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		 mPager = (ViewPager) findViewById(R.id.pager);
	   
	}
	
	
	private class LoadData extends AsyncTask{

		@Override
		protected Object doInBackground(Object... arg0) {
			String url ="http://catfacts-api.appspot.com/api/facts?number=5";
			new Communicator().HTTPGet(url);
			return null;
		}

		@Override
		protected void onPostExecute(Object result) {
			Type listOfResponses = new TypeToken<List<String>>(){}.getType();
			final List<String> _list = gson.fromJson((String) result, listOfResponses);
			 mPager.setAdapter(mPagerAdapter);
			 mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(), null);
			 mPager.setAdapter(mPagerAdapter);
			 gson= new Gson();
			     
			     mPager.setOnPageChangeListener(new OnPageChangeListener(){

					@Override
					public void onPageScrollStateChanged(int arg0) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onPageScrolled(int arg0, float arg1, int arg2) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void onPageSelected(int arg0) {
						// TODO Auto-generated method stub
						
					}
			    	 
			    	 
			     });
			super.onPostExecute(result);
		}
		
		
	}
	
	
	 /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
      
    	List<String>  list;
    	public ScreenSlidePagerAdapter(FragmentManager fm ,List<String> _list ) {
            
    		super(fm);
            this.list= _list;
        }

        @Override
        public Fragment getItem(int position) {
            return new ScreenSlidePageFragment(list.get(position));
        }

        @Override
        public int getCount() {
            return PAGES;
        }
    }
}
