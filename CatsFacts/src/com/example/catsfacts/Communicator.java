package com.example.catsfacts;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.util.Log;

public class Communicator {

	
	public String HTTPGet(String _URL){
		
		String response ="";
			try {
		        HttpClient client = new DefaultHttpClient();  
		        String getURL = _URL;
		        HttpGet get = new HttpGet(getURL);
		     
		        HttpResponse responseGet = client.execute(get);  
		        HttpEntity resEntityGet = responseGet.getEntity();  
		        if (resEntityGet != null) {  
		                    //Response
		                   
		                    response = EntityUtils.toString(resEntityGet);
		                    Log.i("GET RESPONSE",response);
		                }
				} catch (Exception e) {
					e.printStackTrace();
					
				}
			return response;
			
		}
	
	public void HTTPost(){
		
		try {
	        HttpClient client = new DefaultHttpClient();  
	        String postURL = "http://somepostaddress.com";
	        HttpPost post = new HttpPost(postURL); 
	            List<NameValuePair> params = new ArrayList<NameValuePair>();
	          // params.add(new BasicNameValuePair("user", "kris"));
	          //params.add(new BasicNameValuePair("pass", "xyz"));
	            UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params,HTTP.UTF_8);
	            post.setEntity(ent);
	            HttpResponse responsePOST = client.execute(post);  
	            HttpEntity resEntity = responsePOST.getEntity();  
	            if (resEntity != null) {    
	                Log.i("RESPONSE",EntityUtils.toString(resEntity));
	            }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		
		
	}
		
		}